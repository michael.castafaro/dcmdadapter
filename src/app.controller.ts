///                     © Commonwealth of Australia 2022
///                 This file is part of the FIMS DM project
///                                 OFFICIAL

import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
