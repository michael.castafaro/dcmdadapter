///                     © Commonwealth of Australia 2022
///                 This file is part of the FIMS DM project
///                                 OFFICIAL

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(3000);
}
bootstrap();
