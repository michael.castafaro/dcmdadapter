///                     © Commonwealth of Australia 2022
///                 This file is part of the FIMS DM project
///                                 OFFICIAL

import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }
}
