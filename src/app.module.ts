///                     © Commonwealth of Australia 2022
///                 This file is part of the FIMS DM project
///                                 OFFICIAL

import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
