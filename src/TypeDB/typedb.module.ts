///                     © Commonwealth of Australia 2022
///                 This file is part of the FIMS DM project
///                                 OFFICIAL

import { Module } from "@nestjs/common";



@Module({
    imports: [],
    controllers: [],
    providers: [],
  })
  export class TypeDBModule {}